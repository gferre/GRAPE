#---------------------------------------------------------------
#------------ Parameters for the algorithm ---------------------
#---------------------------------------------------------------



#--------- Database parameters -----------#

#Type of kernel
#set_kernel='Coulomb'
set_kernel='GRAPE'


#Define training and testing size
Training_size = 100
Test_size = 100
#Choose one of the five sets
Training_set=0

#----------- Graph parameters --------------#

#Length scale and cut-off for central atom
#(see file adjacency.py)
sigmaC=1.
Rcut=5.

#Length scale and cut-off for neighbors
sigma = 1.
rcut=5.

#number of particles kept in each local environment
n_cutoff=4

#Angular parameter (not used now)
beta=0.
    
#---------- Kernel parameters --------------#

#Regularization parameter (lambda)
Reg=1.e-12
#Exponential coefficient
gamma=10.

