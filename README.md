# GRAPE CODE 

This is a basic version of the so-called GRAPE algorithm used to fit molecular
properties, described in the paper "Learning Potential Energy Landscapes using
Graph Kernel", by G. Ferré, T. Haut and K. Barros, available on arXiv.
Note that the code is very simple, and the main task is to read properly the
QM7 database, given with the code in the file qm7.dat. We briefly describe the
functions of the different files.
Note that this is a very basic and non-optimized version of the code, which is
not the one used for the paper.

### NB
The file qm7.dat should be downloaded for example from
http://quantum-machine.org/datasets/

### How to use
Simply set the qm7.dat file in the directory, and run the main.py file.
This goes through both training and testing, and write the result (expected
value and estimated value of atomization energy) in the output file.

#### main.py
This is the main file. It essentially consists in calling the functions to read
the database, train the model, and test it on a test set. For this train set,
the exact and approximated energies are returned in the file output.

#### parameters.py
This files contains all the parameters for the model (lengthscale, cut-offs,
gamma, etc) and the size of the training and test sets.
Note that one can also set the kernel used to be the Coulomb kernel.

#### build_qm_potential.py
This is the most complex file. First, it reads the prescribed parts of the
database, and store them properly. Then, the eigendecompositions are all
precomputed to speed up the calculations. Finally, the model is trained
(i.e. we compute the K matrix and invert the linear system) and the
approximated energy function is defined.

#### adjacency.py
Here are defined the functions to return the matrix associated with a
configuration, whether it is a GRAPE or a Coulomb matrix.

#### coulomb_kernel.py
Here is defined the Coulomb kernel.

#### graph_kernel.py
Here is defined the GRAPE kernel.

#### gplot
Short file to print the output using gnuplot.
