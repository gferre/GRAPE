import numpy as np
from numpy import linalg as lina
import math
from scipy import sparse
from scipy.sparse import linalg


#--- Correlation function between two atoms
def corr_func(r,param,rcut):
    return np.exp(-r*r*param)*(1+math.cos(math.pi*r/rcut))*0.5
    #return np.exp(-r*r*param)



#--- Coulomb matrix
def Coulomb_matrix(C,Elements):

    [n,d]=C.shape


    #Compute the coulomb matrix
    M=np.zeros((n,n))
    

    #Neighboring atoms
    for i in range(0,n):
        for j in range (0,i):
            rij= math.sqrt(np.dot(C[i,]-C[j,],C[i,]-C[j,]))
            M[i,j]=Z(Elements[i])*Z(Elements[j])/rij

            M[j,i]=M[i,j]

    for i in range (0,n):
        M[i,i]=Z(Elements[i])*Z(Elements[i])

  
    return M



#--- Adjacency matrix for the beta skeleton
#--- for now beta is not taken into account ...
def Beta_adjacency_matrix(C,sigma,sigmaC,rcut,Rcut,beta,Elements):

    [n,d]=C.shape

    eta=1/(2*sigma*sigma)
    xi=1/(2*sigmaC*sigmaC)

    #Compute the adjacency matrix
    A=np.zeros((n,n))



    #Neighboring atoms
    for i in range(0,n):
        for j in range (0,i):
            rij= math.sqrt(np.dot(C[i,]-C[j,],C[i,]-C[j,]))
            if rij<rcut :
                A[i,j]=corr_func(rij,eta,rcut)/(Elements[i]*Elements[i])
                #A[i,j]=Elements[i]*Elements[j]*corr_func(rij,eta,rcut)
            else:
                A[i,j]=0


            A[j,i]=A[i,j]
        A[i,i]=1/Elements[i]*Elements[i]
    
    #--- To be modified if one wants to take into account
    #--- a central atom
    for i in range (0,n):
        ri0= math.sqrt(np.dot(C[i,]-C[0,],C[i,]-C[0,]))
        if ri0<Rcut:
            #A[i,0]=Elements[0]*Elements[i]*corr_func(ri0,xi,rcut)
            A[i,0]=corr_func(ri0,xi,Rcut)/(Elements[0]*Elements[i])
        

        A[0,i]=A[i,0] 

    
    #A[0,0]=Elements[0]


    return A



