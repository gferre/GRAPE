import numpy as np
from numpy import linalg as lina
import math
from scipy import sparse
from scipy.sparse import linalg

from adjacency import Beta_adjacency_matrix, Coulomb_matrix



#-------------------------------------------------------------#
#--------------- Exponential graph kernel --------------------#
#-------------------------------------------------------------#


def Coulomb_kernel (C1,C2,Elements1,Elements2):


    #Dimensions
    (n1,d)=C1.shape
    (n2,d)=C2.shape

    n=max(n1,n2)

    #Compute the first Coulomb matrix
    S1=Coulomb_matrix(C1,Elements1)
        
    D1, P1 = lina.eigh(S1)
    V1=np.zeros(n)

    for i in range(0,n1):
        V1[i]=D1[i]

    #Compute the second Coulomb matrix
    S2=Coulomb_matrix(C2,Elements2)

    D2, P2 = lina.eigh(S2)
    V2=np.zeros(n)

    for i in range(0,n2):
        V2[i]=D2[i]


    V1.sort()
    V2.sort()

    K= math.exp(- (V1-V2).dot(V1-V2) )
        

    return K


