#-----------------------------------------------------
#---------- This file reads the QM7 database ---------
#--------- and construct the numerical potential -----
#-----------------------------------------------------



import numpy as np
from numpy import linalg as lina
import math
import sys 
import scipy.io as sio


from graph_kernel import Graph_kernel
from coulomb_kernel import Coulomb_kernel
from parameters import sigma, sigmaC,rcut, Rcut, Reg, gamma, beta, Training_size, Test_size, Training_set, set_kernel, n_cutoff
from adjacency import Beta_adjacency_matrix,  Coulomb_matrix


#--------------------------------------------------------
#----- Auxilary functions needed for localized GRAPE ---#
#--------------------------------------------------------


#--- Return a local configuration attached with one atom
def get_local_config(Config, Elements, atom_num, n_cutoff):
    n = Config.shape[0]
    Rs = np.zeros(n)

    for j in range(n):
        R = Config[atom_num,:] - Config[j,:]
        Rs[j] = np.sqrt(np.dot(R,R))


    #---This should be change if one wants to use
    # environments with a varying number of atoms
    ind = np.argsort(Rs)[0:n_cutoff]

    return Config[ind,:], Elements[ind]


#--- Index matrix for training the potential
def compute_global_index_matrix(atom_numbers):
   # n_mol = Training_size
    n_mol = atom_numbers.size
    n_train = sum(atom_numbers)
    M = np.zeros((n_train,n_mol))
    k = 0
    for j in range(n_mol):
        n_atom = atom_numbers[j]
        M[k:k+n_atom,j] = 1.
        k = k + n_atom
    return M




#------------------------------------------------------#
#------------ Reading the QM7 database ----------------#
#------------------------------------------------------#


DB = sio.loadmat('qm7.mat')

#--- Different lists
potentials=[]
Configuration_list=[]
Element_list=[]
Atoms_number=[]


[nb_config,a,b]=DB['R'].shape

#--- reading the database
for i in range(0,nb_config):
    
        
    Nb_atoms=0
    p=0
    length=DB['Z'][i].shape[0]

    while p < length:
        if DB['Z'][i][p] != 0:
            Nb_atoms+=1
        p+=1
            
    elements=DB['Z'][i][0:(Nb_atoms)]

    conf=[]

    for j in range(0,Nb_atoms):

        positions=[]
        positions.append(DB['R'][i][j][0])
        positions.append(DB['R'][i][j][1])
        positions.append(DB['R'][i][j][2])
        positions=np.asarray(positions)
        conf.append(positions)

    conf=np.asarray(conf)


    Configuration_list.append(conf)
    potentials.append(DB['T'][0][i])
    Element_list.append(elements)
    Atoms_number.append(Nb_atoms)
        
#----------------------------------------------------
    

Database_size=len(Configuration_list) 


Configurations=np.asarray(Configuration_list)
Potentials=np.asarray(potentials)
Atoms_number=np.asarray(Atoms_number)

print('Database size ',Database_size)


#--- Parameters
eta=1/(2*sigma*sigma)
xi=1/(2*sigmaC*sigmaC)


#--- We split the database in two parts
# for training and testing

Train_Configuration=[]
Test_Configuration=[]

Train_Potentials=[]
Test_Potentials=[]

Train_Elements=[]
Test_Elements=[]

#This is for precomputations
Diag_list=[]
P_list=[]
L1_list=[]
invL1_list=[]


#Choose one of the five sets for the qm7 database
[set_size]=DB['P'][Training_set].shape


# stores the number of atoms in each of the training molecules
atom_numbers = np.zeros(Training_size, dtype='int')


#
#  Precompute eigenvalue decompositions for all local adjacency matrices
#
for i in range(0,Training_size):
    Config=Configurations[DB['P'][Training_set][i]]
    Elements=Element_list[DB['P'][Training_set][i]]
    Train_Configuration.append(Config)
    Train_Potentials.append(Potentials[DB['P'][Training_set][i]])
    Train_Elements.append(Elements)
    
    #pre-calculation for configurations of the DB
  
    (n,d)=Config.shape
    # store number of atoms in the ith training molecule (used for)
    # forming the regression matrix
    atom_numbers[i] = n
    if set_kernel == 'GRAPE':
    	for i_local in range(n):
            # Get local configuration, i.e. the n_cutoff closest atoms to atom i_local
            Config_Local, Elements_Local = \
                get_local_config(Config, Elements, i_local, n_cutoff)
            #Adjacency matrix for local graph
            A=Beta_adjacency_matrix(Config_Local,sigma,sigmaC,rcut,Rcut,beta,Elements_Local)
            #diagonalization
            Diag, P = lina.eigh(A)
            Diag_list.append(np.diag(Diag))
            P_list.append(P)
  
            #initial and stopping probabilities
            p=np.ones(n_cutoff)
            q=np.ones(n_cutoff)
    
            #multiplication of these probabilities
            #(right and left elements for q^T*exp(A)*p
            L1=q.dot(P)
            invL1= lina.solve(P,p)

            L1_list.append(L1)
            invL1_list.append(invL1)



#--- Testing configurations
for i in range(set_size-Test_size,set_size):
    Test_Configuration.append(Configurations[DB['P'][Training_set][i]])
    Test_Potentials.append(Potentials[DB['P'][Training_set][i]])
    Test_Elements.append(Element_list[DB['P'][Training_set][i]])







#------------------------------------------------------------#
#----- Building the correlation matrix and the potential ----#
#------------------------------------------------------------#

#################################
#--- NB: choose the kernel !!---#
#--- (parameter "set_kernel") --#
#################################

K=np.zeros((Training_size,Training_size))

Training_size_all = sum(atom_numbers)
K_big=np.zeros((Training_size_all,Training_size_all))
print('Size of the large training set: ',Training_size_all)
print('Size of the test set: ',Test_size)

#-------------------------------------#
#--- Training with potential energy --#
#-------------------------------------#


#--- Training for GRAPE
if set_kernel == 'GRAPE':    
    for i in range(0,Training_size_all):
        for j in range(0,i):
            K_big[i,j]=Graph_kernel(Diag_list[i],L1_list[i],invL1_list[i],Diag_list[j],L1_list[j],invL1_list[j],gamma)

            K_big[j,i]=K_big[i,j]
            
        K_big[i,i]=1

    # form small regression matrix for total energy from big regression matrix for
    # local atomic energies
    M = compute_global_index_matrix(atom_numbers)
    K = np.dot(K_big, M)
    K = np.dot(np.transpose(M), K)
        

#--- Training for Coulomb
if set_kernel == 'Coulomb':    
    for i in range(0,Training_size):
        for j in range(0,i):
            K[i,j]=Coulomb_kernel(Train_Configuration[i],Train_Configuration[j],Train_Elements[i],Train_Elements[j])
        
            K[j,i]=K[i,j]
            
        K[i,i]=1






#--- Linear system to find the alpha vector   

I_db=np.eye(Training_size)
#
alpha=lina.solve(K+Training_size*Reg*I_db,Train_Potentials)
if set_kernel == 'GRAPE':    
	alpha = np.dot(M,alpha)


print('Training done.')



#---------------------------------------------------#
#--- Random walk graph kernel for localized GRAPE --#
#--------- Approximated energy function ------------#
#---------------------------------------------------#

def NumPotGraph(C,Elements,alpha,Diag_list,L1_list,invL1_list,\
                Train_Elements,Training_size,sigma,sigmaC,rcut,\
                Rcut,gamma,beta,atom_numbers,n_cutoff):
    
    Training_size_all = sum(atom_numbers)
    n_star = C.shape[0]
    K = np.zeros((Training_size_all, n_star))
    #output: approximated value of the energy
    val=0
    
    # Precompute eigenvalue decompositions of local adjacency 
    # matrix for configuration C
    Diag_list_Local=[]
    P_list_Local=[]
    L1_list_Local=[]
    invL1_list_Local=[]
    n_star = Elements.size
    for i_local in range(n_star):
        Config_Local, Elements_Local = \
             get_local_config(C, Elements, i_local, n_cutoff)
        #Adjacency matrix for local graph
        A=Beta_adjacency_matrix(Config_Local,sigma,sigmaC,rcut,Rcut,beta,Elements_Local)
        #diagonalization
        Diag, P = lina.eigh(A)
        Diag_list_Local.append(np.diag(Diag))
        P_list_Local.append(P)
        #initial and stopping probabilities
        p=np.ones(n_cutoff)
        q=np.ones(n_cutoff)

        #multiplication of these probabilities
        #(right and left elements for q^T*exp(A)*p
        L1=q.dot(P)
        invL1= lina.solve(P,p)
        L1_list_Local.append(L1)
        invL1_list_Local.append(invL1)

    #--- Computation of the approximated value
    for i in range(0,Training_size_all): 
        for i_local in range(0,n_star):
            K[i,i_local] = Graph_kernel(Diag_list[i], L1_list[i], invL1_list[i],\
                                        Diag_list_Local[i_local], L1_list_Local[i_local], \
                                        invL1_list_Local[i_local], gamma)
    val = sum(np.dot(np.transpose(K),alpha))
    
    return val




#--------------------------------------------
#--- Coulomb kernel


def CoulombPot(C,Elements,alpha,Configuration,Train_Elements,Training_size):
    val=0
    
    for i in range(0,Training_size):
        corr=Coulomb_kernel(Configuration[i],C,Train_Elements[i],Elements)
        val+=corr*alpha[i]

    return val

