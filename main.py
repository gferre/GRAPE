#-----------------------------------------------------------
#--------------- Main file for numerical potential ---------
#------------------- using kernels and graphs --------------

#external librairies
import numpy as np
from numpy import linalg as lina
import math
import timeit
import sys


#other files
from graph_kernel import Graph_kernel
from build_qm_potential import Potentials, Configurations, Train_Configuration, NumPotGraph, alpha, Database_size, \
Training_size, Test_size, Test_Potentials, Test_Configuration, Train_Elements, Test_Elements, CoulombPot, \
Diag_list, P_list, L1_list, invL1_list, atom_numbers, n_cutoff


from parameters import sigma, sigmaC, rcut, Rcut, gamma, beta, Reg, set_kernel



#--- Write the scatter plot for the training set
#--- in output file

Potential_compare=open('Potential_compare','w')
output=open('output','w')
Error=0
MSE=0
mean_pot=0

for l in range(0,Test_size):
   #--- Approximation

   if set_kernel == 'GRAPE':
   	  approx=NumPotGraph(Test_Configuration[l],Test_Elements[l],alpha,Diag_list,\
   	  	                 L1_list,invL1_list, Train_Elements,Training_size,sigma,sigmaC,\
   	  	                 rcut,Rcut,gamma,beta,atom_numbers,n_cutoff)

   if set_kernel == 'Coulomb':
      approx=CoulombPot(Test_Configuration[l],Test_Elements[l],alpha,Train_Configuration,Train_Elements,Training_size)


   output.write(str(Test_Potentials[l]))
   output.write(' ')
   output.write(str(approx))
   output.write(' ')

   output.write("\n")
   
   Error+=math.fabs(Test_Potentials[l]-approx)
   MSE+=(Test_Potentials[l]-approx)*(Test_Potentials[l]-approx)
   

print('Regularization parameter lambda: ',Reg)
print('Gamma: ', gamma)
print('Sigma central atom: ',sigmaC)
print('Sigma neighbors: ', sigma)
print('Rcut ', Rcut)
print('rcut: ', rcut)
print('Number of neighbors: ', n_cutoff)
print('Mean average error: ',(Error/float(Test_size)))
print('Root mean square error: ',(math.sqrt(MSE/float(Test_size))))
print('Method used: ', set_kernel)



